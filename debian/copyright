Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: qmljsreformatter
Upstream-Contact: Julien Déramond <julien.deramond@orange.com>
Source: https://github.com/Orange-OpenSource/qmljsreformatter

Files: qt-creator-simplified/*
Copyright: 2016 The Qt Company Ltd.
License: GPL3-QTEXCEPT

Files: qt-creator-simplified/src/libs/utils/ansiescapecodehandler.*
Copyright: 2016 Petar Perisin <petar.perisin@gmail.com>
License: GPL3-QTEXCEPT

Files: qt-creator-simplified/src/libs/utils/fuzzymatcher.*
Copyright: 2017 The Qt Company Ltd.
           2017 BlackBerry Limited <qt@blackberry.com>
           2017 Andre Hartmann <aha_1980@gmx.de>
License: GPL3-QTEXCEPT

Files: qt-creator-simplified/src/libs/utils/shellcommand.*
Copyright: 2016 Brian McGillion and Hugues Delorme
License: GPL3-QTEXCEPT

Files: qt-creator-simplified/src/libs/utils/completinglineedit.*
       qt-creator-simplified/src/libs/utils/globalfilechangeblocker.*
Copyright: Orgad Shaneh <orgads@gmail.com>
License: GPL3-QTEXCEPT

Files: qt-creator-simplified/src/libs/utils/theme/theme.*
       qt-creator-simplified/src/libs/utils/theme/theme_p.h
Copyright: Thorben Kroeger <thorbenkroeger@gmail.com>
License: GPL3-QTEXCEPT

Files: qt-creator-simplified/src/libs/3rdparty/cplusplus/*
Copyright: 2008 Roberto Raggi <roberto.raggi@gmail.com>
License: MIT

Files: qt-creator-simplified/src/libs/3rdparty/cplusplus/Matcher.*
       qt-creator-simplified/src/libs/3rdparty/cplusplus/SafeMatcher.*
       qt-creator-simplified/src/libs/3rdparty/cplusplus/cppassert.h
Copyright: 2016 The Qt Company Ltd.
License: GPL3-QTEXCEPT

Files: qt-creator-simplified/src/libs/3rdparty/optional/*
Copyright: 2011-2012 Andrzej Krzemienski
License: Boost-1.0

Files: src/*
Copyright: 2017-2019 Orange
License: GPL3-QTEXCEPT

Files: debian/*
Copyright: 2020 Mathieu Mirmont <mat@parad0x.org>
License: MIT

License: MIT
 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the
 "Software"), to deal in the Software without restriction, including
 without limitation the rights to use, copy, modify, merge, publish,
 distribute, sublicense, and/or sell copies of the Software, and to
 permit persons to whom the Software is furnished to do so, subject to
 the following conditions:
 .
 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

License: GPL3-QTEXCEPT
 This is the GNU General Public License version 3, annotated with The
 Qt Company GPL Exception 1.0:
 .
 Exception 1:
 As a special exception you may create a larger work which contains
 the output of this application and distribute that work under terms
 of your choice, so long as the work is not otherwise derived from or
 based on this application and so long as the work does not in itself
 generate output that contains the output from this application in its
 original or modified form.
 .
 Exception 2:
 As a special exception, you have permission to combine this
 application with Plugins licensed under the terms of your choice, to
 produce an executable, and to copy and distribute the resulting
 executable under the terms of your choice. However, the executable
 must be accompanied by a prominent notice offering all users of the
 executable the entire source code to this application, excluding the
 source code of the independent modules, but including any changes you
 have made to this application, under the terms of this license.
 .
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or (at
 your option) any later version.
 .
 This program is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.

License: Boost-1.0
 Boost Software License - Version 1.0 - August 17th, 2003
 .
 Permission is hereby granted, free of charge, to any person or organization
 obtaining a copy of the software and accompanying documentation covered by
 this license (the "Software") to use, reproduce, display, distribute,
 execute, and transmit the Software, and to prepare derivative works of the
 Software, and to permit third-parties to whom the Software is furnished to
 do so, all subject to the following:
 .
 The copyright notices in the Software and this entire statement, including
 the above license grant, this restriction and the following disclaimer,
 must be included in all copies of the Software, in whole or in part, and
 all derivative works of the Software, unless such copies or derivative
 works are solely in the form of machine-executable object code generated by
 a source language processor.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT
 SHALL THE COPYRIGHT HOLDERS OR ANYONE DISTRIBUTING THE SOFTWARE BE LIABLE
 FOR ANY DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT, TORT OR OTHERWISE,
 ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 DEALINGS IN THE SOFTWARE.
